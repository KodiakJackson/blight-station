/obj/item/clothing/glasses/sunglasses/lenses
	name = "small sun lenses"
	desc = "A small pair of tinted lenses to protect sensitive eyes from bright light"
	item_icons = list(slot_glasses_str = 'icons/mob/species/teshari/onmob_eyes_teshari.dmi')
	icon = 'icons/obj/clothing/species/teshari/obj_eyes_teshari.dmi'
	icon_state = "sun_lenses"
	item_state = null
	species_restricted = list(SPECIES_TESHARI)
	flash_protection = FLASH_PROTECTION_MODERATE
	body_parts_covered = 0

/obj/item/clothing/glasses/sunglasses/sechud/lenses
	name = "small sechud lenses"
	desc = "A small pair of tinted lenses to protect sensitive eyes from bright light. These ones have a security HUD installed in them"
	item_icons = list(slot_glasses_str = 'icons/mob/species/teshari/onmob_eyes_teshari.dmi')
	icon = 'icons/obj/clothing/species/teshari/obj_eyes_teshari.dmi'
	icon_state = "sec_lenses"
	item_state = null
	species_restricted = list(SPECIES_TESHARI)
	flash_protection = FLASH_PROTECTION_MODERATE

/obj/item/clothing/glasses/hud/health/lenses
	name = "small medhud lenses"
	desc = "A small pair of tinted lenses to protect sensitive eyes from bright light. These ones can tell the wearer the health status of those in view."
	item_icons = list(slot_glasses_str = 'icons/mob/species/teshari/onmob_eyes_teshari.dmi')
	icon = 'icons/obj/clothing/species/teshari/obj_eyes_teshari.dmi'
	icon_state = "med_lenses"
	item_state = null
	flash_protection = FLASH_PROTECTION_MODERATE
	species_restricted = list(SPECIES_TESHARI)
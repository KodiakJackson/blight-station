/obj/item/clothing/shoes/workboots/teshari
	name = "small workboots"
	icon = 'icons/obj/clothing/species/teshari/obj_feet_teshari.dmi'
	icon_state = "teshari_workboots"
	item_state = "workboots"
	desc = "Small and tight shoes with an iron sole for those who work in dangerous area or hate their paws"
	w_class = ITEM_SIZE_SMALL
	species_restricted = list(SPECIES_TESHARI)

/obj/item/clothing/shoes/workboots/teshari/New()
	..()
	slowdown_per_slot[slot_shoes] = 0.3

/obj/item/clothing/shoes/footwraps
	name = "cloth footwraps"
	icon = 'icons/obj/clothing/species/teshari/obj_feet_teshari.dmi'
	desc = "A roll of treated canvas used for wrapping paws"
	icon_state = "clothwrap"
	item_state = "clothwrap"
	force = 0
	item_flags = ITEM_FLAG_SILENT
	w_class = ITEM_SIZE_SMALL
	species_restricted = list(SPECIES_TESHARI)
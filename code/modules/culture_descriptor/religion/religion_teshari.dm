/decl/cultural_info/religion/teshari
	name = RELIGION_TESHARI_SPIRITS
	description = "Teshari packs that follow this religion believe that every living being has \
	a spirit, and that when a being dies, their spirit go to join the 'World-Spirits'.\
	World-Spirts are believed to be in control of natural things that happen around \
	the universe, such as a few Spirits being in control of something such as a mountain or a space station.\
	\
	A follower of Teshari Spiritualism would believe that a natural disaster may be because \
	the World-Spirits are angry, while a bountiful harvest may be from the spirits being happy\
	As such, keeping the World-Spirits satisfied is the primary intent of most rituals within \
	this religion."
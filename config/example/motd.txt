<h1>Welcome to Space Station 13!</h1>

-<i>This server is running <a href="https://gitlab.com/blight-station/blight-station-13/">Blight Station 13's</a> modification of the <a href="http://baystation12.net/">Baystation 12</a> SS13 code.</i>
<br><br>
<strong>Bugtracker:</strong> <a href="http://baystation12.net/forums/threads/issue-tracker-report-template.110/">for posting of bugs and issues.</a>

/datum/map/untold
	name = "\improper Alderis"
	full_name = "\improper Alderis Station"
	path = "untold"
	flags = MAP_HAS_BRANCH | MAP_HAS_RANK
	config_path = "config/untold_config.txt"

	admin_levels = list(7)
	empty_levels = list(9)
	accessible_z_levels = list("1"=1,"2"=3,"3"=1,"4"=1,"5"=1,"6"=1,"9"=30)
	overmap_size = 20
	overmap_event_areas = 19
	usable_email_tlds = list("alderis.ec.scg", "alderis.fleet.mil", "freemail.net", "alderis.scg")

	allowed_spawns = list("Cryogenic Storage", "Cyborg Storage")
	default_spawn = "Cryogenic Storage"

	station_name  = "\improper Alderis Station"
	station_short = "\improper Alderis"
	dock_name     = "TBD"
	boss_name     = "Expeditionary Command"
	boss_short    = "Command"
	company_name  = "NanoTrasen"
	company_short = "NanoTrasen"

	map_admin_faxes = list(
		"Unknown Signal",
		"Local Government Services"
	)

	//These should probably be moved into the evac controller...
	shuttle_docked_message = "Attention all hands: Jump preparation complete. The bluespace drive is now spooling up, secure all stations for departure. Time to jump: approximately %ETD%."
	shuttle_leaving_dock = "Attention all hands: Jump initiated, exiting bluespace in %ETA%."
	shuttle_called_message = "Attention all hands: Jump sequence initiated. Transit procedures are now in effect. Jump in %ETA%."
	shuttle_recall_message = "Attention all hands: Jump sequence aborted, return to normal operating conditions."

	evac_controller_type = /datum/evacuation_controller/starship

	default_law_type = /datum/ai_laws/solgov
	use_overmap = 1
	num_exoplanets = 1

	away_site_budget = 3
	id_hud_icons = 'maps/untold/icons/assignment_hud.dmi'

/obj/effect/overmap/visitable/ship/untold
	name = "Alderis Station"
	desc = "An ancient nearly defunct NanoTrasen mobile outpost station. It's still broadcasting standard NTS codes and the designation \"NSB Alderis, NCR-10X-KMO-0\"."
	fore_dir = NORTH
	vessel_mass = 100000
	burn_delay = 2 SECONDS
	base = TRUE

	initial_restricted_waypoints = list(
		"Trichoptera" = list("nav_trichoptera_altdock"), //restricts Trichoptera specific docking waypoint on deck 4 portside
		"Skrellian Scout" = list("nav_skrellscout_dock"), //restricts Skrell Scoutship specific docking waypoint on deck 4 portside
		"Rescue" = list("nav_ert_dock"), //restricts ERT Shuttle specific docking waypoint on deck 4 portside
		"ITV The Reclaimer" = list("nav_hangar_gantry_torch"), //gantry shuttles
		"ITV Vulcan" = list("nav_hangar_gantry_torch_two"),
		"ITV Spiritus" = list("nav_hangar_gantry_torch_three"),
		"SRV Venerable Catfish" = list("nav_verne_5"), //docking location for verne shuttle
		"New Hope" = list("nav_hangar_calypso"), // Exploration Shuttle
		"Iron Hammer" = list("nav_hangar_mining") // Mining vessel
	)

	initial_generic_waypoints = list(
	)

//Exploration Ship
/obj/effect/overmap/visitable/ship/landable/exploration_shuttle
	name = "New Hope"
	desc = "A barely working NSV Redlon class vessel, broadcasting NTS codes and the callsign \"Alderis-1 New Hope\"."
	shuttle = "New Hope"
	max_speed = 1/(2 SECONDS)
	burn_delay = 1 SECONDS
	vessel_mass = 5000
	fore_dir = WEST
	skill_needed = SKILL_BASIC
	vessel_size = SHIP_SIZE_SMALL

/obj/machinery/computer/shuttle_control/explore/exploration_shuttle
	name = "shuttle control console"
	shuttle_tag = "New Hope"
	req_access = list(access_expedition_shuttle_helm)

//Mining Ship
/obj/effect/overmap/visitable/ship/landable/mining_vessel
	name = "Iron Hammer"
	desc = "An old Sol-Gov Salvager class vessel, broadcasting NTS codes and the callsign \"Alderis-1 Iron Hammer\"."
	shuttle = "Iron Hammer"
	max_speed = 1/(2 SECONDS)
	burn_delay = 2 SECONDS
	vessel_mass = 750
	fore_dir = WEST
	skill_needed = SKILL_BASIC
	vessel_size = SHIP_SIZE_SMALL

/obj/machinery/computer/shuttle_control/explore/mining_vessel
	name = "shuttle control console"
	shuttle_tag = "Iron Hammer"
	req_access = list(access_mining)